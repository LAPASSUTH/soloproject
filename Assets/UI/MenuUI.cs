﻿using UnityEngine;

public class MenuUI : MonoBehaviour
{
    void Start()
    {
        GameManager.Instance.Begin();
    }

    void Update()
    {
        GameManager.Instance.OnPlay();
    }

    public void StartButton()
    {
        GameManager.Instance.PlayTheGame();
    }

    public void RestartButton()
    {
        GameManager.Instance.RePlayTheGame();
    }

    public void BackToMainMenuButton()
    {
        GameManager.Instance.QuitTheGame();
    }
}
