﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBullet : MonoBehaviour
{
    [SerializeField] private AudioClip playerLaserSound;
    [SerializeField] private AudioClip enemyLaserSound;
    [SerializeField] internal int damage;
    private float volume = 1f;
    public Rigidbody ammo;
    internal Rigidbody laserSplit;
    [SerializeField] private float speed;

    public void laser()
    {
        laserSplit = Instantiate
            (ammo, transform.position, transform.rotation);

        laserSplit.AddForce(0, speed, 0);
        AudioSource.PlayClipAtPoint(playerLaserSound, gameObject.transform.position, volume);


    }

    public void enemylaser()
    {
        laserSplit = Instantiate
            (ammo, transform.position, transform.rotation);

        laserSplit.AddForce(0, -speed, 0);
        AudioSource.PlayClipAtPoint(enemyLaserSound, gameObject.transform.position, volume);


    }
}
