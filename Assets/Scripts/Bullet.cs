﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private AudioClip playerBulletSound;
    [SerializeField] private AudioClip playerLaserSound;
    [SerializeField] private AudioClip enemyBulletSound;
    [SerializeField] internal int damage;
    private float volume = 1f;
    public Rigidbody normalAmmo;
    public Rigidbody laserAmmo;
    internal Rigidbody bulletSplit;
    [SerializeField] private float speed;

    public void bullet()
    {
        bulletSplit = Instantiate
            (normalAmmo, transform.position, transform.rotation);
        
        bulletSplit.AddForce(0, speed, 0);
        AudioSource.PlayClipAtPoint(playerBulletSound, gameObject.transform.position, volume);
        

    }

    public void enemybullet()
    {
        bulletSplit = Instantiate
            (normalAmmo, transform.position, transform.rotation);

        bulletSplit.AddForce(0, -speed, 0);
        AudioSource.PlayClipAtPoint(enemyBulletSound, gameObject.transform.position, volume);


    }

    public void laser()
    {
        bulletSplit = Instantiate
            (laserAmmo, transform.position, transform.rotation);

        bulletSplit.AddForce(0, 250 , 0);
        AudioSource.PlayClipAtPoint(playerLaserSound, gameObject.transform.position, volume);
    }

}
