﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using EnemyShip;


namespace PlayerShip
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private AudioClip hitSound;
        [SerializeField] private AudioClip destroySound;
        [SerializeField] internal float hp;
        [SerializeField] internal int damage;
        [SerializeField] private Bullet bullet;
        [SerializeField] private float playerShipSpeed = 10;
        [SerializeField] private GameObject gameObject;
        [SerializeField] private EnemyController enemy;
        [SerializeField] private EnemyController secEnemy;
        [SerializeField] private EnemyController thirdEnemy;
        [SerializeField] private Transform enemyObject;
        internal Vector3 playerPosition;
        internal Vector3 enemyPosition;
        internal bool laser;
        internal float volume;

        
        private Vector3 movementInput = Vector2.zero;

        private float xMin;
        private float xMax;
        private float yMin;
        private float yMax;
        private float padding = 1;

        private void Start()
        {
            laser = false;
            SetupMoveBoundaries();
            playerPosition = new Vector3(gameObject.transform.position.x
                , gameObject.transform.position.y, gameObject.transform.position.z);
        }
        private void Update()
        {
            Move();
            
        }
        private void SetupMoveBoundaries()
        {
            Camera gameCamera = Camera.main;
            xMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).x + padding;
            xMax = gameCamera.ViewportToWorldPoint(new Vector2(1, 0)).x - padding;
            
            yMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).y + padding;
            yMax = gameCamera.ViewportToWorldPoint(new Vector2(0, 1)).y - padding;
        }
        private void Move()
        {
            var inputDirection = movementInput.normalized;
            var inPutVelocity = inputDirection * playerShipSpeed;
            
            var finalVelocity = inPutVelocity;

            var newXPos = transform.position.x + finalVelocity.x * Time.deltaTime;
            var newYPos = transform.position.y + finalVelocity.y * Time.deltaTime;

            newXPos = Mathf.Clamp(newXPos, xMin, xMax);
            newYPos = Mathf.Clamp(newYPos, yMin, yMax);
            
            transform.position = new Vector2(newXPos, newYPos);
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            movementInput = context.ReadValue<Vector2>();
        }

        public void Shoot()
        {

            if (laser)
            {
                bullet.laser();
            }
            else
            {
                bullet.bullet();
            }
        }

        private void OnTriggerEnter(Collider other)
        {

            hp -= enemy.bulletDamage.damage;
            ScoreManager.Instance.scoreMinus(10);

            AudioSource.PlayClipAtPoint(hitSound, gameObject.transform.position, volume = 1.5f);

            if (hp <= 0)
            {
                AudioSource.PlayClipAtPoint(destroySound, gameObject.transform.position, volume = 1.5f);
                gameObject.SetActive(false);
                gameObject.transform.position = playerPosition;
                enemy.gameObject.transform.position = enemy.enemyPosition;
                secEnemy.gameObject.transform.position = secEnemy.enemyPosition;
                thirdEnemy.gameObject.transform.position = thirdEnemy.enemyPosition;

            }

        }

    }
}
