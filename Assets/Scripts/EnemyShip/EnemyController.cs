﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using PlayerShip;
using UnityEngine.UI;

namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private AudioClip destroySound;
        [SerializeField] internal Bullet bulletDamage;
        [SerializeField] internal float hp;
        [SerializeField] private Transform playerTransform;
        [SerializeField] private float enemyShipSpeed = 5;
        [SerializeField] private Renderer playerRenderer;
        [SerializeField] private GameObject gameObject;
        [SerializeField] private PlayerController Player;
        [SerializeField] private float fireRate;
        [SerializeField] private GameObject playerObject;
        internal float volume;
        float timeCounter;
        private Vector3 playerPosition;
        internal Vector3 enemyPosition;
        internal int Score;
        private float chasingThresholdDistance = 1.0f;
        private Renderer enemyRenderer;

        private void Awake()
        {
           
            enemyRenderer = GetComponent<Renderer>();
            
        }

        void Update()
        {
            MoveToPlayer();
            timeCounter += Time.deltaTime;
            if (gameObject == true)
            {
                if (timeCounter >= fireRate)
                {
                    bulletDamage.enemybullet();
                    timeCounter = 0;
                }
            }

            /*if (Vector3.Distance(playerObject.transform.position, transform.position) < 1f)
            {
                timeCounter = fireRate;
            }*/
        }

        private void OnDrawGizmos()
        {
            CollisionDebug();
        }
        private void CollisionDebug()
        {
            if (enemyRenderer != null && playerRenderer != null)
            {
                if (intersectAABB(enemyRenderer.bounds, playerRenderer.bounds))
                {
                    Gizmos.color = Color.red;
                    
                }
                else
                {
                    Gizmos.color = Color.white;
                }
                Gizmos.DrawWireCube(enemyRenderer.bounds.center, 2 * enemyRenderer.bounds.extents);
                Gizmos.DrawWireCube(playerRenderer.bounds.center, 2 * playerRenderer.bounds.extents);
            }


        }
        private bool intersectAABB(Bounds a, Bounds b)
        {
            return ((a.min.x <= b.max.x && a.max.x >= b.min.x) &&
                    (a.min.y <= b.max.y && a.max.y >= b.min.y));

        }
        private void MoveToPlayer()
        {
            Vector3 enemyPosition = transform.position;
            Vector3 playerPosition = playerTransform.position;
            enemyPosition.z = playerPosition.z; // ensure there is no 3D rotation by aligning Z position

            Vector3 vectorToTarget = playerPosition - enemyPosition; // vector from this object towards the target location
            Vector3 directionToTarget = vectorToTarget.normalized;
            Vector3 velocity = directionToTarget * enemyShipSpeed;

            float distanceToTarget = vectorToTarget.magnitude;

            if (distanceToTarget > chasingThresholdDistance)
            {
                transform.Translate(velocity * Time.deltaTime);
            }

        }

        private void OnTriggerEnter(Collider other)
        {
            hp -= Player.damage;
            ScoreManager.Instance.scoreCounting(Player.damage + 15);

            if (other.gameObject.CompareTag("Player"))
            {
                Player.hp -= 100;
                ScoreManager.Instance.scoreMinus(100);
            }


            if (hp <= 0)
            {
                AudioSource.PlayClipAtPoint(destroySound, gameObject.transform.position, volume = 1.5f);
                gameObject.SetActive(false);
                gameObject.transform.position = enemyPosition;
                
                
            }

        }

        void Start()
        {
            enemyPosition = new Vector3(gameObject.transform.position.x,
            gameObject.transform.position.y, gameObject.transform.position.z);
        }
    }
}
