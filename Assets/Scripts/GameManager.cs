﻿using EnemyShip;
using PlayerShip;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using GameManage;


public class GameManager : MonoSingleton<GameManager>
{
    private AudioSource audioPlayer;
    [SerializeField] private AudioClip backgroundMusic;
    [SerializeField] private GameObject startButton;
    [SerializeField] private GameObject quit;
    [SerializeField] private PlayerController player;
    [SerializeField] private EnemyController enemy;
    [SerializeField] private EnemyController secEnemy;
    [SerializeField] private EnemyController thirdEnemy;
    private float enemyHP;
    private float playerHP;
    public Text ScoreBoard;
    public Text showScoreOnPlay;


    public void Begin()
    {
        showScoreOnPlay.gameObject.SetActive(false);
        audioPlayer = GetComponent<AudioSource>();
        audioPlayer.loop = true;
        audioPlayer.clip = backgroundMusic;
        audioPlayer.volume = 0.2f;
        audioPlayer.Play();
        enemyHP = enemy.hp;
        playerHP = player.hp;
        player.gameObject.SetActive(false);
        enemy.gameObject.SetActive(false);
        secEnemy.gameObject.SetActive(false);
        thirdEnemy.gameObject.SetActive(false);
        quit.gameObject.SetActive(false);
        startButton.gameObject.SetActive(true);
        

    }

    public void OnPlay()
    {
        
        showScoreOnPlay.text = $"Score : {ScoreManager.Instance.showScore()}";

        if (enemy.hp <= 0)
        {
           
            secEnemy.gameObject.SetActive(true);
            thirdEnemy.gameObject.SetActive(true);
            player.gameObject.SetActive(true);
            enemy.gameObject.SetActive(false);
            player.laser = true;
            enemy.hp = enemyHP;


        }

        if (secEnemy.hp <= 0)
        {
            secEnemy.gameObject.SetActive(false);
        }

        if (thirdEnemy.hp <= 0)
        {
            thirdEnemy.gameObject.SetActive(false);
        }

        if (secEnemy.hp <= 0 && thirdEnemy.hp <= 0)
        {
            ScoreBoard.text = $"Score : {ScoreManager.Instance.showScore()}";
            player.gameObject.SetActive(false);
            quit.gameObject.SetActive(true);

        }

        if (player.hp <= 0)
        {
            
            ScoreBoard.text = $"Score : {ScoreManager.Instance.showScore()}";
            quit.gameObject.SetActive(true);
            player.gameObject.SetActive(false);
            enemy.gameObject.SetActive(false);
            secEnemy.gameObject.SetActive(false);
            thirdEnemy.gameObject.SetActive(false);
            showScoreOnPlay.gameObject.SetActive(false);
            

        }

    }

    public void PlayTheGame()
    {
        audioPlayer.Stop();
        audioPlayer.Play();
        showScoreOnPlay.gameObject.SetActive(true);
        showScoreOnPlay.text = $"Score : {ScoreManager.Instance.showScore()}";
        quit.gameObject.SetActive(false);
        startButton.gameObject.SetActive(false);
        player.gameObject.SetActive(true);
        enemy.gameObject.SetActive(true);
        secEnemy.gameObject.SetActive(false);
        thirdEnemy.gameObject.SetActive(false);
        
    }

    public void RePlayTheGame()
    {
        ScoreManager.Instance.scoreReset();
        PlayTheGame();
        enemy.hp = enemyHP;
        secEnemy.hp = enemyHP;
        thirdEnemy.hp = enemyHP;
        player.hp = playerHP;
        player.transform.position = player.playerPosition;
        player.laser = false;
    }

    public void QuitTheGame()
    {
        ScoreManager.Instance.scoreReset();
        player.transform.position = player.playerPosition;
        enemy.hp = enemyHP;
        secEnemy.hp = enemyHP;
        thirdEnemy.hp = enemyHP;
        player.hp = playerHP;
        player.laser = false;
        audioPlayer.Stop();
        Begin();

    }
}
