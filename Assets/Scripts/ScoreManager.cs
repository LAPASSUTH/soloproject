﻿using System.Collections;
using System.Collections.Generic;
using GameManage;
using UnityEngine;

public class ScoreManager : MonoSingleton<ScoreManager>
{
    public int score = 0;
   
    public void scoreCounting(int counter)
    {
        score += counter;
    }

    public void scoreMinus(int counter)
    {
        score -= counter;
    }

    public int showScore()
    {
        return score;
    }

    public int scoreReset()
    {
        return score = 0;
    }
}
